﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Procore_Coding_Project.Battleship;

namespace ProcoreTests
{
    [TestClass]
    public class BattleshipTests
    {

        [TestCategory("Battleship")]
        [TestMethod]
        public void TestGame()
        {
            Game game = new Game();

            Procore_Coding_Project.Battleship.Pages.Placement.Game = game; // Some variables rely on the static Game from this page, so set it to this
            

            game.NewGame();
            game.Player1Board.AddBattleship(game.Player1Board.Tiles[4, 4], Direction.Right);
            game.Player1Board.AddSubmarine(game.Player1Board.Tiles[3, 3], Direction.Right);
            game.Player1Board.AddDestroyer(game.Player1Board.Tiles[8, 1], Direction.Down);
            game.Player2Board.AIPlaceShips();

            // Go through the placement process
            game.NextPlacement();
            game.NextPlacement();
            game.NextPlacement();
            game.NextPlacement();

            game.NextTurn();

            // Test to make sure all ships are in place
            Assert.IsNotNull(game.Player1Board.Player.Battleship, "User battleship is null");
            Assert.IsNotNull(game.Player1Board.Player.Destroyer, "User destroyer is null");
            Assert.IsNotNull(game.Player1Board.Player.Sub, "User sub is null");

            // Ensure that the "ships sank" is 0
            Assert.IsTrue(game.Player1Board.Player.ShipsSank == 0, "Incorrect number of ships have sunk.");

            // Ensure that the tiles are correct
            Assert.IsTrue(game.Player1Board.Tiles[5, 4].TileState == Tile.State.Placed, "Player battleship tile not correct state");
            Assert.IsTrue(game.Player2Board.Player.Battleship.Tiles[0].TileState == Tile.State.Occupied, "AI battleship tile not correct state");

            // Ensure that no one has one
            Assert.IsFalse(game.Player1Board.Player.HasWon(), "Game says player has won, player has not");



            //Make a move
            Tile t = game.Player2Board.Player.Destroyer.Tiles[0];
            // Ensure we have the correct board
            Assert.AreEqual(game.Player2Board.Tiles[t.X, t.Y], t, "Tiles (t) are not equal");

            game.Fire(t.X, t.Y);

            // Make sure that we fired correctly
            Assert.IsTrue(t.TileState == Tile.State.Hit, "Fired tile was not hit correctly");

            //Now, let them make a move and miss us
            game.NextTurn();

            Tile t2 = game.Player1Board.Tiles[0, 0];
            Assert.IsTrue(t2.TileState == Tile.State.Empty, "User tiles aren't set up correctly");
            Assert.AreEqual(game.Player1Board.Tiles[t2.X, t2.Y], t2, "Tiles (t2) are not equal");

            // Fire
            game.Fire(0, 0);
            Assert.IsTrue(t2.TileState == Tile.State.EmptyHit, "Fire did not work correctly on User's board");


            // Now our turn. Sink their destroyer
            game.NextTurn();
            Tile t3 = game.Player2Board.Player.Destroyer.Tiles[1];
            Assert.IsTrue(t3.TileState == Tile.State.Occupied);
            Assert.AreEqual(game.Player2Board.Tiles[t3.X, t3.Y], t3, "Tiles (t3) are not equal");

            game.Fire(t3.X, t3.Y);

            // Ensure that we sank their ship
            Assert.IsTrue(t3.TileState == Tile.State.Sank, "Ship tile was not placed as 'sunk'");
            Assert.IsTrue(t.TileState == Tile.State.Sank, "First hit tile didn't change to 'sunk'");
            Assert.IsTrue(game.Player2Board.Player.ShipsSank == 1, "Did not sink ship when we should have");

        }
    }
}
