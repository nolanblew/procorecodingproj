﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Procore_Coding_Project;
using System.Threading.Tasks;
using System.Linq;

namespace ProcoreTests
{
    [TestClass]
    public class PrimeTests
    {
        [TestMethod]
        public async Task TestLowPrimes()
        {
            // Prime source: https://en.wikipedia.org/wiki/List_of_prime_numbers#The_first_500_prime_numbers

            // Test first 20 primes
            var list = await Eratosthenes.FindPrimes(20);
            Assert.IsTrue(list.Count == 8, "Incorrect count for primes up to 20");
            Assert.IsTrue(list.Except(new int[] { 2, 3, 5, 7, 11, 13, 17, 19 }).Count() == 0, "Primes not correct up to 20");
            
            // Test first 100 primes
            list = await Eratosthenes.FindPrimes(100);
            Assert.IsTrue(list.Count == 25, "Incorrect count for primes up to 100");
            Assert.IsTrue(list.Except(new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 }).Count() == 0, "Primes not correct up to 100");
        }

        [TestMethod]
        public async Task TestPrimes()
        {
            // Do a reverse iteration to ensure each prime can only be multiplied by itself and 1
            // For sake of time, only run up to 100,000 primes
            var list = await Eratosthenes.FindPrimes(100000);
            foreach(var prime in list)
            {
                int sqrt = (int)Math.Sqrt(prime);
                for (int i = 2; i <= sqrt; i++)
                    if (prime % i == 0)
                        Assert.Fail("Found number that is not prime.", prime, i);
            }
            
        }
    }
}
