﻿using Procore_Coding_Project.Battleship.Pages;
using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Procore_Coding_Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
            frame.Navigate(new Startup());
        }

        public Storyboard GetStoryboard(string name)
        {
            return (Storyboard)TryFindResource(name);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
