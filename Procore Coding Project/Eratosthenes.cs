﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Procore_Coding_Project
{
    public class Eratosthenes
    {

        public static async Task<List<int>> FindPrimes(int max)
        {
            return await Task.Run(() =>
            {
                // Hash to hold the "marked" and "unmarked" values
                Dictionary<int, bool> list = new Dictionary<int, bool>();

                // Populate the hash, marking all numbers as true
                for (int i = 2; i <= max; i++)
                    list.Add(i, true);

                // Loop through all of the values to find the ones that are not prime
                for (int i = 2; i <= Math.Ceiling(Math.Sqrt(max)); i++)
                {
                    // This values has been marked as prime, so continue
                    if (list[i])
                    {
                        // Go through and mark all of it's multiples to false, i.e. not prime
                        int mult = 0;
                        int j = i * i;
                        while (j <= max)
                        {
                            list[j] = false;
                            mult++;
                            j = (i * i) + (mult * i);
                        }
                    }
                }

                // Return all the values left as true, or prime
                return list.Where(l => l.Value).Select(l => l.Key).ToList();
            });
        }

    }
}
