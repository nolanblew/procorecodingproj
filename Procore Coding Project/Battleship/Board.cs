﻿using Procore_Coding_Project.Battleship.Pieces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Procore_Coding_Project.Battleship
{
    public class Board
    {
        public Board(bool SetupBoard = true)
        {
            if (SetupBoard)
                ClearBoard();
        }

        public Tile[,] Tiles = new Tile[64, 64];
        public Player Player { get; set; } = new Player();

        #region Board Methods

        void ClearBoard()
        {
            Tiles = new Tile[Game.BoardSize, Game.BoardSize];
            for (int y = 0; y < Game.BoardSize; y++)
                for (int x = 0; x < Game.BoardSize; x++)
                    Tiles[x, y] = new Tile(x, y);

        }


        public void AddDestroyer(Tile tile, Direction direction)
        {
            Player.Destroyer = new Destroyer(tile, direction, this, Player);
        }

        public void AddSubmarine(Tile tile, Direction direction)
        {
            Player.Sub = new Submarine(tile, direction, this, Player);
        }

        public void AddBattleship(Tile tile, Direction direction)
        {
            Player.Battleship = new Pieces.Battleship(tile, direction, this, Player);
        }



        public bool Fire(int x, int y)
        {
            if (!Tiles[x, y].IsOccupied() || Tiles[x,y].Ship == null)
            {
                Tiles[x,y].TileState = Tile.State.EmptyHit;
                return false;
            }

            if (Tiles[x, y].TileState == Tile.State.Occupied || Tiles[x, y].TileState == Tile.State.Placed)
            {
                Tiles[x, y].Ship.Hit(x, y);
                return Tiles[x,y].TileState == Tile.State.Hit || Tiles[x,y].TileState == Tile.State.Sank;
            }

            return false;
        }


        public Tile this[int x, int y]
        {
            get { return Tiles[x, y]; }
            set { Tiles[x, y] = value; }
        }

        #endregion




        #region AI Variables

        private HashSet<Coordinate> hitCoordinates = new HashSet<Coordinate>();

        // Variables for AI to look for ship after it has been hit
        private bool _lookingForShip = false;
        private Direction _lookingDirection = Direction.Left;
        private bool _lastDirectionSuccess = false;
        private Coordinate? _lastHitCoordinate;
        private Coordinate? _originalHitCoordinate;
        private int _lastShipSinkCount = 0;
        private bool _tryingReverse = false;

        public static int MaxPrime = 120;

        #endregion

        #region Artificial Intelligence Methods

        Random _rnd = new Random();

        /// <summary>
        /// Has the AI randomly place ships along the route
        /// </summary>
        public void AIPlaceShips()
        {

            for (int i = 0; i < 3; i++)
            {
                Direction direction = (Direction)_rnd.Next(0, 4);

                bool error = true;
                int count = 0;

                while (error && count < 25)
                {
                    try
                    {
                        // Get random X and Y coordinates
                        int x = _rnd.Next(0, Game.BoardSize);
                        int y = _rnd.Next(0, Game.BoardSize);

                        // Attempt to place the ship
                        if (i == 0)
                            AddBattleship(Tiles[x, y], direction);
                        if (i == 1)
                            AddSubmarine(Tiles[x, y], direction);
                        if (i == 2)
                            AddDestroyer(Tiles[x, y], direction);

                        error = false;
                    }
                    catch
                    {
                        error = true;
                    }
                    finally
                    {
                        count++;
                    }
                }

            }
        }

        /// <summary>
        /// The AI computer will attempt to fire on your board
        /// </summary>
        public void AIFire()
        {

            Coordinate nextCoordinate = new Coordinate(-1, -1);

            // Check to see if we are looking for a particular direction after we hit a ship
            if (_lookingForShip)
            {
                if (_lastDirectionSuccess)
                {
                    nextCoordinate = _lastHitCoordinate.Value + _lookingDirection.ToCoordinate();

                    // Check to see if we tried reversing and failed
                    if (_tryingReverse && hitCoordinates.Contains(nextCoordinate))
                    {
                        // Probably a sideways ship. For purposes of this demo, just stop looking for the ship alltogether.
                        _tryingReverse = false;
                        _lastDirectionSuccess = false;
                        _lastHitCoordinate = null;
                        _lookingForShip = false;
                    }

                }
                else
                {
                    // The last direction was NOT a success, so try again
                    _lookingDirection = NextDirection(_lookingDirection);

                    // Cycle through each driection until we get one that hasn't been used/tested before
                    int count = 0;
                    nextCoordinate = _lastHitCoordinate.Value + _lookingDirection.ToCoordinate();

                    while (nextCoordinate.X >= 0 && nextCoordinate.X < Battleship.Game.BoardSize &&
                           nextCoordinate.Y >= 0 && nextCoordinate.Y < Battleship.Game.BoardSize &&
                           hitCoordinates.Contains(nextCoordinate) && count < 4)
                    {
                        _lookingDirection = NextDirection(_lookingDirection);
                        nextCoordinate = _lastHitCoordinate.Value + _lookingDirection.ToCoordinate();
                        count++;
                    }

                    if (count >= 4) // We went all aroudn our given coordinates and there were no ship tiles, so scratch looking and go back to randomizing
                        _lookingForShip = false;
                }
            }

            if (!_lookingForShip)
            {
                nextCoordinate = new Coordinate(_rnd.Next(0, Game.BoardSize), _rnd.Next(0, Game.BoardSize));

                while (hitCoordinates.Contains(nextCoordinate))
                    // This tile has already been fired on. Don't do it again.
                    nextCoordinate = new Coordinate(_rnd.Next(0, Game.BoardSize), _rnd.Next(0, Game.BoardSize));

            }

            if (Fire(nextCoordinate.X, nextCoordinate.Y))
            {
                // Save the last hit location
                _lastHitCoordinate = nextCoordinate;

                // We hit something. Let's see if we were already tracking a ship or not
                if (!_lookingForShip)
                {
                    // We originally weren't looking for a ship. Let's add this ship to our list
                    _lookingForShip = true;
                    _lookingDirection = GetRandomDirection();
                    _lastDirectionSuccess = false;
                    _originalHitCoordinate = nextCoordinate;
                }
                else
                {
                    // We are already looking for a ship
                    // Check if we sank the ship
                    if (Player.ShipsSank > _lastShipSinkCount)
                    {
                        // We sank the ship. Clear looking for a ship and move on
                        _lookingForShip = false;
                        _lastHitCoordinate = null;
                        _lastShipSinkCount = Player.ShipsSank;
                    }
                    else
                    {
                        // This time it was a success. Make sure the last direction we looked at is saved
                        _lastDirectionSuccess = true;
                    }
                }
            }
            else
            {
                // We fired and missed. See what we can do
                if (_lookingForShip && _lastDirectionSuccess)
                {
                    // Seems like we reached the wrong end. Let's go back and try again, but in the opposite direction from our initial hit point
                    _lastHitCoordinate = _originalHitCoordinate;
                    _lookingDirection = NextDirection(NextDirection(_lookingDirection)); // Should reverse direction
                }
            }

            hitCoordinates.Add(nextCoordinate);
        }


        Direction NextDirection(Direction direction)
        {
            if (direction == Direction.Left) direction = Direction.Up;
            else if (direction == Direction.Up) direction = Direction.Right;
            else if (direction == Direction.Right) direction = Direction.Down;
            else if (direction == Direction.Down) direction = Direction.Left;

            return direction;
        }

        Direction GetRandomDirection()
        {
            switch (_rnd.Next(0, 4))
            {
                case 0:
                    return Direction.Left;
                case 1:
                    return Direction.Up;
                case 2:
                    return Direction.Right;
                case 3:
                    return Direction.Down;
            }

            return Direction.Left;
        }

        #endregion

        #region AI Messaging

        // Folder path
        string messagePath = "Battleship/EnemyMessages.txt";
        List<string> messages = new List<string>();

        public string GetRandomEnemyMessage()
        {
            if (messages == null || messages.Count == 0) PopulateMessages();

            return DecodeMessage(messages[_rnd.Next(0, messages.Count)]);

        }

        void PopulateMessages()
        {
            messages = new List<string>();

            foreach (var line in File.ReadAllLines(messagePath))
            {
                if (!string.IsNullOrWhiteSpace(line) && line.Substring(0, 2) != @"//")
                    messages.Add(line);
            }
        }

        string DecodeMessage(string message)
        {

            // Check to see if this is a "use once" message
            if (message.First() == '*')
            {
                messages.Remove(message);
                message = message.Substring(1);
            }

            // Replace the message componenets with the coordinates and shipname
            string shipString = "Destroyer";
            Pieces.ShipBase ship = Player.Destroyer;

            switch (_rnd.Next(0, 3))
            {
                case 0:
                    shipString = "Battleship";
                    ship = Player.Battleship;
                    break;
                case 1:
                    shipString = "Destroyer";
                    ship = Player.Destroyer;
                    break;
                case 2:
                    shipString = "Sub";
                    ship = Player.Sub;
                    break;
            }

            message = message.Replace("[rn]", shipString);
            message = message.Replace("[r]", GetRandomCoordinate(ship).ToString());

            // Replace for battleship
            message = message.Replace("[b]", GetRandomCoordinate(Player.Battleship).ToString());
            // Replace for sub
            message = message.Replace("[s]", GetRandomCoordinate(Player.Sub).ToString());
            // Replace for destroyer
            message = message.Replace("[d]", GetRandomCoordinate(Player.Destroyer).ToString());

            return message;
        }

        Coordinate GetRandomCoordinate(Pieces.ShipBase ship)
        {
            // Don't return a square that has been hit
            var notHitList = ship.Tiles.Where(t => t.TileState != Tile.State.Hit);
            int placed = _rnd.Next(0, notHitList.Count());

            // Add (1,1) to make it 1-based instead of 0-based
            return new Coordinate(ship.Tiles[placed].X, ship.Tiles[placed].Y) + new Coordinate(1,1);
        }

        #endregion

    }

    public enum Direction
    {
        Up, Down, Left, Right
    }
}
