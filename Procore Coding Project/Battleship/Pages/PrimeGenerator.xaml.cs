﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace Procore_Coding_Project.Battleship.Pages
{
    /// <summary>
    /// Interaction logic for PrimeGenerator.xaml
    /// </summary>
    public partial class PrimeGenerator : Page, INotifyPropertyChanged
    {

        List<int> keys = new List<int>();

        public PrimeGenerator()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await FindTokens(false);
        }


        private async void btnAutoFind_Click(object sender, RoutedEventArgs e)
        {
            if (!await FindTokens(true))
                return;

            // Attempts to see if one of the primes is the private key. Won't say which one though
            int pKey = -1;
            foreach (int key in keys)
            {
                if (Encryption.IsPrivateKey(key))
                {
                    pKey = key;
                    break;
                }
            }

            if (pKey != -1)
                // Found the key
                // Send the "decode message" segment to the screen
                DecodeMessage?.Invoke(this, null);
            else
                // Could not find the key
                MessageBox.Show("Sorry, we couldn't crack their private key. Try again!", "Failed");

            
            // Close the loading screen
            grdLoading.Visibility = Visibility.Collapsed;

        }

        private void txtPrime_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtPrime.Background = new SolidColorBrush(Colors.White);
            if (!string.IsNullOrEmpty(txtPrime.Text))
            {
                int num;
                IsInputCorrect(out num);
            }
        }

        async Task<bool> FindTokens(bool isAutoFind)
        {
            // Find all the primes!
            // Check for correct input
            int number;
            if (!IsInputCorrect(out number, isAutoFind, true))
                return false;

            // Show the loading screen
            grdLoading.Visibility = Visibility.Visible;

            try
            {

                // Do the calculations
                List<int> primes = await Eratosthenes.FindPrimes(number);

                // Set the UI list
                if (!isAutoFind)
                {
                    lstPrimes.Items.Clear();
                    primes.ForEach(p => lstPrimes.Items.Add(p));
                    lstPrimes.Visibility = Visibility.Visible;
                } else
                {
                    keys.Clear();
                    primes.ForEach(p => keys.Add(p));
                }

            }
            catch (Exception ex)
            {
                // There was some sort of error during the calculation or setting of the UI elements.
                MessageBox.Show("An error has occured! Please try again.");

                if (Debugger.IsAttached)
                {
                    Debug.WriteLine("Error: " + ex.Message);
                    throw;
                }
            }

            // Reset the textbox
            txtPrime.Text = "";
            txtPrime.Focus();

            // Clear the loading screen, IF WE ARE NOT ON AUTO FIND
            if (!isAutoFind)
                grdLoading.Visibility = Visibility.Collapsed;

            return true;
        }

        /// <summary>
        /// Check to ensure that there is valid input
        /// </summary>
        /// <param name="input">The input that was typed in. -1 if false</param>
        /// <returns>True if the input was valid, false otherwise</returns>
        private bool IsInputCorrect(out int input, bool isAutoFind = false, bool gameComfirmation = false)
        {
            // Store the text into a variable
            string txt = txtPrime.Text.Trim();

            // Set input to -1 for a fail event
            input = -1;

            // Lets us know if we passed or failed
            bool passed = true;

            // Check to make sure we put something in the box
            if (string.IsNullOrWhiteSpace(txt))
                passed = false;

            // Check to make sure the input is actually an integer
            else if (!int.TryParse(txt, out input))
                passed = false;

            // Check to make sure it's positive, and less than a super high number
            else if (input < 2 || input >= 10000000)
                passed = false;

            // Check to make sure we don't go above the game's max limit
            else if (IsGame && input > Board.MaxPrime)
                passed = false;

            // If we passed, return true. Otherwise set the box to red and return false.
            if (!passed)
            {
                txtPrime.Background = new SolidColorBrush(Colors.Red);
                txtPrime.Focus();
            }

            if (IsGame && passed)
            {
                int neededTokens = 0;
                if (!isAutoFind)
                    neededTokens = (3 + (int)((input - 1) / 40));
                else
                    neededTokens = (3 + (int)((input - 1) / 40)) * 2;

                passed = Player.Tokens >= neededTokens;


                if (gameComfirmation) // We clicked the button
                {
                    if (!passed)
                        MessageBox.Show($"You don't have enough tokens! You'll need {neededTokens} tokens to complete this operation.\nYou have {Player.Tokens} tokens.", "Not enough tokens!", MessageBoxButton.OK, MessageBoxImage.Error);
                    else
                        // Remove the tokens from the player
                        Player.Tokens -= neededTokens;
                }
            }

            return passed;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                MainWindow window = Window.GetWindow(this) as MainWindow;
                window.BeginStoryboard(window.GetStoryboard("AnimToStart"));
                NavigationService.GoBack();
            }
            else
            {
                Window.GetWindow(this).Close();
            }
        }

        #region Game Properties

        public static readonly DependencyProperty IsGameProperty = DependencyProperty.Register("IsGame", typeof(bool), typeof(PrimeGenerator), new PropertyMetadata(false));
        public bool IsGame
        {
            get { return (bool)GetValue(IsGameProperty); }
            set { SetValue(IsGameProperty, value); }
        }

        private Player _player;
        public Player Player
        {
            get { return _player; }
            set
            {
                if (_player != value)
                {
                    _player = value;
                    OnPropertyChanged("Player");
                }
            }
        }

        void SetTokens()
        {
            if (Player.Tokens < 3)
            {
                txtPrime.Text = "";
                txtPrime.IsEnabled = false;
                btnFind.IsEnabled = false;
                btnAutoFind.IsEnabled = false;
            }
            else
            {
                txtPrime.Text = "";
                txtPrime.IsEnabled = true;
                btnFind.IsEnabled = true;
                btnAutoFind.IsEnabled = Player.Tokens >= 6;
            }
        }

        public event EventHandler DecodeMessage;

        #endregion

        #region Property Changed Events

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        #endregion

    }
}
