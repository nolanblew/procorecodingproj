﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace Procore_Coding_Project.Battleship.Pages
{
    /// <summary>
    /// Interaction logic for Startup.xaml
    /// </summary>
    public partial class Startup : Page
    {

        public Startup()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PrimeGenerator pg = new PrimeGenerator();
            pg.IsGame = false;

            MainWindow window = Window.GetWindow(this) as MainWindow;
            window.BeginStoryboard(window.GetStoryboard("AnimToPrimes"));
            NavigationService.Navigate(pg);
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = Window.GetWindow(this) as MainWindow;
            window.BeginStoryboard(window.GetStoryboard("AnimToBattleship"));
            NavigationService.Navigate(new Placement());
        }
    }
}
