﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace Procore_Coding_Project.Battleship.Pages
{
    public class TileColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() != typeof (Battleship.Tile.State)) return new SolidColorBrush(Colors.White);
            
            Battleship.Tile.State state = (Battleship.Tile.State) value;

            if (state == Battleship.Tile.State.Empty) return new SolidColorBrush(Colors.White);
            else if (state == Battleship.Tile.State.EmptyHit) return new SolidColorBrush(Colors.LightSkyBlue);
            else if (state == Battleship.Tile.State.Hit) return new SolidColorBrush(Colors.Red);
            else if (state == Battleship.Tile.State.Sank) return new SolidColorBrush(Colors.DarkRed);
            else if (state == Battleship.Tile.State.TestPlace) return new SolidColorBrush(Colors.Yellow);
            else if (state == Battleship.Tile.State.Placed) return new SolidColorBrush(Colors.DarkOrange);

            return new SolidColorBrush(Colors.White);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BoolVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;
            
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
                return ((Visibility)value == Visibility.Visible);

            return false;
        }
    }
}
