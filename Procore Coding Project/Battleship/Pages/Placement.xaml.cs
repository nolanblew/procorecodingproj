﻿using Procore_Coding_Project.Battleship.Pieces;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace Procore_Coding_Project.Battleship.Pages
{
    /// <summary>
    /// Interaction logic for Placement.xaml
    /// </summary>
    public partial class Placement : Page
    {
        private ObservableCollection<ObservableCollection<Battleship.Tile>> sources;

        public static Game Game;

        private Direction _currentPlacingDirection = Direction.Left;
        private string _currentEncryptedMessage;
        private Coordinate? _lastMouseLocation;

        private PrimeGeneratorWindow primeGeneratorWindow = new PrimeGeneratorWindow();
        protected PrimeGenerator primeGeneratorPage = new PrimeGenerator();

        private int decrypt_tokens
        {
            get { return Game.Player2Board.Player.Tokens; }
            set { Game.Player2Board.Player.Tokens = value; }
        }


        public Placement()
        {
            InitializeComponent();
            Application.Current.MainWindow.KeyUp += MainWindow_KeyUp;
            WindowTitle = "Battleship";

            Game = new Game();
            Game.NewGame();
            SetContext(Game.Player1Board);

            GetKeys();

            stkStatus.DataContext = Game.Player2Board.Player;
        }


        private void PlacementPage_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigating += NavigationService_Navigating;
        }

        private void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back) // Leaving
            {
                if (primeGeneratorWindow.IsInitialized)
                    primeGeneratorWindow.Close();

                e.Cancel = true;
                NavigationService.Navigate(new Startup());

            }
        }
        

        async void GetKeys()
        {
            await Encryption.GeneratePublicPrivateKey();
            txtPublicKey.Text = "Public key: " + Encryption.PublicKey;
        }

        public void SetContext(Board gameBoard)
        {

            sources = gameBoard.Tiles.ToDoubleObservableCollection();
            itemsPanel.ItemsSource = sources;
        }

        private async void Tile_OnClicked(object sender, TileEventArgs e)
        {
            // Handle event for placing if we are in the pre-stages
            if (Game.IsPlacement)
            {
                try
                {
                    Game.SelectedShipToPlace?.Dispose();
                    if (Game.ShipSelect == 0)
                        // Place the destroyer
                        Game.Player1Board.AddDestroyer(e.Tile, _currentPlacingDirection);
                    if (Game.ShipSelect == 1)
                        // Place the sub
                        Game.Player1Board.AddSubmarine(e.Tile, _currentPlacingDirection);
                    if (Game.ShipSelect == 2)
                        // Place the battleship
                        Game.Player1Board.AddBattleship(e.Tile, _currentPlacingDirection);


                    NextPlacement();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Error: " + ex.Message);
                }
            }
            else if (Game.IsActive)
            {
                // Handle turn events for clicking on a tile

                // First, ensure that the tile we are clicking on is empty.
                if (!lastState.HasValue || (lastState.Value != Battleship.Tile.State.Empty && lastState.Value != Battleship.Tile.State.Occupied && lastState.Value != Battleship.Tile.State.Placed))
                    return;

                e.Tile.TileState = lastState.Value; // Reset the tile state to the actual state it should be
                lastState = null;
                Battleship.Game.isLogging = true;

                bool hit = Game.Fire(e.Tile.X, e.Tile.Y);

                if (!hit)
                    decrypt_tokens++;
                else
                    decrypt_tokens += 2;

                await Task.Delay(TimeSpan.FromSeconds(2)); // Show the user if they hit or missed anything

                NextTurn();

            }
        }


        private void PrimeGeneratorButton_Click(object sender, RoutedEventArgs e)
        {
            if (primeGeneratorWindow != null && primeGeneratorWindow.IsVisible) return; // Window already exists and is Visible 

            primeGeneratorWindow = new PrimeGeneratorWindow();
            primeGeneratorPage = new PrimeGenerator();
            primeGeneratorPage.IsGame = true;
            primeGeneratorPage.Player = Game.Player2Board.Player; // Sets the player holding the tokens (binded to UI)
            primeGeneratorWindow.frameNav.Navigate(primeGeneratorPage);
            primeGeneratorWindow.Show();

            // Attatch the decoded button
            primeGeneratorPage.DecodeMessage += ((s, ev) =>
            {
                string message = _currentEncryptedMessage;
                message = Encryption.Decrypt(message, Encryption.PublicKey);
                blkEncryptedMessage.Text = message;
                txtInstructions.Text = "We figured out their message!";
            });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.KeyUp -= MainWindow_KeyUp;
            NavigationService.GoBack();
        }

        private void DecryptButton_Click(object sender, RoutedEventArgs e)
        {
            if (decrypt_tokens > 0)
            {
                // Validate text
                int testPvt;
                if (!int.TryParse(txtGuessPrivate.Text, out testPvt))
                    return;

                string message = _currentEncryptedMessage;
                message = Encryption.Decrypt(message, testPvt, Encryption.PublicKey);
                blkEncryptedMessage.Text = message;

                decrypt_tokens--;
            }
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            // The next button in the instructions has been clicked.
            if (Game.IsPlacement)
                NextPlacement();

        }

        private void NextPlacement()
        {
            Game.NextPlacement();

            if (Game.ShipSelect == 0)
            {
                btnInstructionNext.Visibility = Visibility.Hidden;
                txtInstructions.Text = "Let's place your Destroyer. The destroyer is 2 squares. Use < or > to rotate and click to place.";
            }
            else if (Game.ShipSelect == 1)
                txtInstructions.Text = "Now let's place your Submarine. The submarine is 3 squares. Use < or > to rotate and click to place.";
            else if (Game.ShipSelect == 2)
                txtInstructions.Text = "Finally, let's place your Battleship. The battleship is 5 squares. Use < or > to rotate and click to place.";
            else if (Game.ShipSelect == 3)
            {
                btnInstructionNext.Visibility = Visibility.Visible;
                txtInstructions.Text = "We are ready for battle captain! Here is your layout. Press Next to begin!";
                Game.Player2Board.AIPlaceShips();
            }
            else if (Game.ShipSelect == 4)
            {
                btnInstructionNext.Visibility = Visibility.Collapsed;
                NextMessage();
                grdMessages.Visibility = Visibility.Visible;
                txtInstructions.Text = "Begin!";
                SetContext(Game.Player2Board);
                NextTurn();
            }
        }

        private async void NextTurn()
        {
            if (!Game.IsPlacement)
            {
                // Check to see if anyone has won
                if (Game.Player2Board.Player.HasWon())
                {
                    txtInstructions.Text = "";
                    blkEndMessage.Text = "You Win!";
                    blkEndDescription.Text = "Congrats captain! Let's grab a drink and get back to sea!";
                    BeginStoryboard((System.Windows.Media.Animation.Storyboard)FindResource("ShowWin"));
                    return;
                }
                if (Game.Player1Board.Player.HasWon())
                {
                    txtInstructions.Text = "";
                    blkEndMessage.Text = "You lost";
                    blkEndDescription.Text = "Captain: I am dissapointed in your defeat. You can't just leave it at that - you need to prove yourself worthy! Go again!";
                    BeginStoryboard((System.Windows.Media.Animation.Storyboard)FindResource("ShowWin"));
                    return;
                }


                Game.NextTurn();

                if (Game.MyTurn)
                {
                    txtInstructions.Text = "Your turn! Bombs away!";
                    Battleship.Game.isLogging = false;
                    SetContext(Game.Player2Board);
                    // My turn
                }
                else
                {
                    SetContext(Game.Player1Board);
                    txtInstructions.Text = "Enemy's turn to fire!";

                    await Task.Delay(TimeSpan.FromSeconds(1));

                    // Their turn
                    Game.Player1Board.AIFire();

                    // Wait a few seconds so the player can see the damage they did to us
                    await Task.Delay(TimeSpan.FromSeconds(2));

                    // Update the enemy's message
                    NextMessage();

                    NextTurn();
                }
            }
        }

        void NextMessage()
        {
            string message = Game.Player2Board.GetRandomEnemyMessage();
            message = Encryption.Encrypt(message);
            blkEncryptedMessage.Text = message;
            _currentEncryptedMessage = message;
        }

        Battleship.Tile.State? lastState = null;
        private void Tile_OnEntered(object sender, TileEventArgs e)
        {
            if (Game.IsPlacement)
            {
                try
                {
                    if (Game.ShipSelect == 0)
                        Game.SelectedShipToPlace = new Destroyer(e.Tile, _currentPlacingDirection, Game.Player1Board, Game.Player1Board.Player, true);
                    else if (Game.ShipSelect == 1)
                        Game.SelectedShipToPlace = new Submarine(e.Tile, _currentPlacingDirection, Game.Player1Board, Game.Player1Board.Player, true);
                    else if (Game.ShipSelect == 2)
                        Game.SelectedShipToPlace = new Pieces.Battleship(e.Tile, _currentPlacingDirection, Game.Player1Board, Game.Player1Board.Player, true);
                }
                catch (Exception ex)
                {
                    Game.SelectedShipToPlace?.Dispose();
                }
            }
            else if (Game.IsActive)
            {
                lastState = e.Tile.TileState;
                e.Tile.TileState = Battleship.Tile.State.TestPlace;
            }

            _lastMouseLocation = new Coordinate(e.Tile.X, e.Tile.Y);
        }

        private void Tile_OnExited(object sender, TileEventArgs e)
        {
            if (Game.IsPlacement && e.Tile.IsOccupied() && e.Tile.TileState == Battleship.Tile.State.TestPlace)
            {
                Game.SelectedShipToPlace.Dispose();
            }
            else if (!Game.IsPlacement && Game.IsActive)
            {
                if (lastState != null)
                    e.Tile.TileState = lastState.Value;

                lastState = null;
            }

            _lastMouseLocation = null;
        }


        // Switch/rotate piece
        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (Game.IsPlacement)
            {
                if (e.Key == Key.OemPeriod) // Clockwise
                {
                    if (_currentPlacingDirection == Direction.Left) _currentPlacingDirection = Direction.Up;
                    else if (_currentPlacingDirection == Direction.Up) _currentPlacingDirection = Direction.Right;
                    else if (_currentPlacingDirection == Direction.Right) _currentPlacingDirection = Direction.Down;
                    else if (_currentPlacingDirection == Direction.Down) _currentPlacingDirection = Direction.Left;
                }
                else if (e.Key == Key.OemComma) // Counter-clockwise
                {
                    if (_currentPlacingDirection == Direction.Left) _currentPlacingDirection = Direction.Down;
                    else if (_currentPlacingDirection == Direction.Down) _currentPlacingDirection = Direction.Right;
                    else if (_currentPlacingDirection == Direction.Right) _currentPlacingDirection = Direction.Up;
                    else if (_currentPlacingDirection == Direction.Up) _currentPlacingDirection = Direction.Left;
                }

                Game.SelectedShipToPlace.Dispose();
                if (_lastMouseLocation.HasValue)
                    Tile_OnEntered(this, new TileEventArgs(Game.CurrentBoard.Tiles[_lastMouseLocation.Value.X, _lastMouseLocation.Value.Y]));
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // Start a new game
            NavigationService.Navigate(new Placement());
        }
    }
}
