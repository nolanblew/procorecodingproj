﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Procore_Coding_Project.Battleship.Pages
{
    /// <summary>
    /// Interaction logic for Tile.xaml
    /// </summary>
    public partial class Tile : UserControl
    {

        public Tile()
        {
            InitializeComponent();
        }

        public Tile(Battleship.Tile tile)
        {
            InitializeComponent();

            // Set the context
            TileContext = tile;
        }

        private void Rectangle_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // Trigger the clicked event
            Clicked?.Invoke(this, new TileEventArgs(TileContext));
        }

        private void Rectangle_MouseEnter(object sender, MouseEventArgs e)
        {
            Entered?.Invoke(this, new TileEventArgs(TileContext));
        }

        private void Rectange_MouseExit(object sender, MouseEventArgs e)
        {
            Exited?.Invoke(this, new TileEventArgs(TileContext));
        }


        #region Properties

        public static readonly DependencyProperty TileContextProperty = DependencyProperty.Register("TileContext", typeof(Battleship.Tile), typeof(Tile));

        public Battleship.Tile TileContext
        {
            get { return (Battleship.Tile)GetValue(TileContextProperty); }
            set { SetValue(TileContextProperty, value); }
        }

        #endregion

        #region Events

        /// <summary>
        /// Triggered when the tile is clicked
        /// </summary>
        public event EventHandler<TileEventArgs> Clicked;

        /// <summary>
        /// Triggered when the mouse has entered the tile
        /// </summary>
        public event EventHandler<TileEventArgs> Entered;

        /// <summary>
        /// Triggered when the mouse has exited the tile
        /// </summary>
        public event EventHandler<TileEventArgs> Exited;

        #endregion
    }

    public class TileEventArgs : EventArgs
    {
        public TileEventArgs(Battleship.Tile tile)
        {
            Tile = tile;
        }

        public Battleship.Tile Tile { get; set; }

    }
}
