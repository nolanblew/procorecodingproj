﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Procore_Coding_Project.Battleship.Pieces;

namespace Procore_Coding_Project.Battleship
{

    public class Tile : INotifyPropertyChanged
    {
        #region Constructors

        /// <summary>
        /// Initiates a new "Tile"
        /// </summary>
        /// <param name="x">The X-coordinate according to the board</param>
        /// <param name="y">The Y-coordinate according to the board</param>
        /// <param name="state">(Optional) The current state of the tile. Default is Empty</param>
        public Tile(int x, int y, State state = State.Empty)
        {
            X = x;
            Y = y;
            TileState = state;
        }

        #endregion

        #region Properties

        public int X { get; set; }
        public int Y { get; set; }

        private State _tileState;
        public State TileState
        {
            get { return _tileState; }
            set
            {
                if (_tileState != value)
                {
                    if (Game.isLogging && value == State.Empty)
                        Debug.WriteLine("UPDATE: Tile (" + X + "," + Y + ") change from " + _tileState.ToString() + " to " + value.ToString());
                    _tileState = value;
                    OnPropertyChanged("TileState");
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Check to see if the square is occupied with a ship
        /// </summary>
        /// <returns>True if a ship is or was there, false if it is empty</returns>
        public bool IsOccupied()
        {
            return TileState != State.Empty;
        }

        public ShipBase Ship { get; set; }

        #endregion

        /// <summary>
        /// Represents the state of the tile
        /// </summary>
        public enum State
        {
            Empty, EmptyHit, Occupied, Hit, Sank, TestPlace, Placed
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

    }

}
