﻿namespace Procore_Coding_Project.Battleship.Pieces
{
    public class Battleship : ShipBase
    {
        public Battleship(Tile startTile, Direction direction, Board board, Player player, bool isTemporaryPlacement = false) :
            base(startTile.X, startTile.Y, direction, board, player, 5, isTemporaryPlacement) { }

        public Battleship(int x, int y, Direction direction, Board board, Player player, bool isTemporaryPlacement = false) :
            base(x, y, direction, board, player, 5, isTemporaryPlacement) { }
    }
}
