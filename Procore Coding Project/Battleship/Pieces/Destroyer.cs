﻿namespace Procore_Coding_Project.Battleship.Pieces
{
    public class Destroyer : ShipBase
    {

        public Destroyer(Tile startTile, Direction direction, Board board, Player player, bool isTemporaryPlacement = false) :
            base(startTile.X, startTile.Y, direction, board, player, 2, isTemporaryPlacement) {}

        public Destroyer(int x, int y, Direction direction, Board board, Player player, bool isTemporaryPlacement = false) :
            base(x, y, direction, board, player, 2, isTemporaryPlacement) { }

    }
}
