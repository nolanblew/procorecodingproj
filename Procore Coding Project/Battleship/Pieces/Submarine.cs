﻿namespace Procore_Coding_Project.Battleship.Pieces
{
    public class Submarine : ShipBase
    {
        public Submarine(Tile startTile, Direction direction, Board board, Player player, bool isTemporaryPlacement = false) :
            base(startTile.X, startTile.Y, direction, board, player, 3, isTemporaryPlacement) { }

        public Submarine(int x, int y, Direction direction, Board board, Player player, bool isTemporaryPlacement = false) :
            base(x, y, direction, board, player, 3, isTemporaryPlacement)
        { }
    }
}
