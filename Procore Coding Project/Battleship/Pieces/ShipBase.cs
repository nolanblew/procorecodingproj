﻿using System;
using System.Linq;
using Procore_Coding_Project.Battleship.Pages;

namespace Procore_Coding_Project.Battleship.Pieces
{
    public class ShipBase : IDisposable
    {

        // The tiles taken by the ship
        public Tile[] Tiles;

        #region Constructors

        public ShipBase() { }

        public ShipBase(int x, int y, Direction direction, Board board, Player player, int size, bool isTemporaryPlacement = false)
        {
            this.Player = player;
            this.Size = size;

            Tiles = new Tile[Size];
            for (int i = 0; i < Size; i++)
            {
                // Check to ensure the ship can fit properly
                if (x < 0 || y < 0 || x >= Game.BoardSize || y >= Game.BoardSize)
                {
                    this.Dispose();
                    throw new Exception("Ship not on board.");
                }

                // Get one of the tiles for the ship
                Tiles[i] = board[x, y];

                if (Tiles[i].IsOccupied())
                {
                    Tiles[i] = null;
                    this.Dispose();
                    throw new Exception("Ship occupying space already taken.");
                }

                // Determine which tile to use
                if (isTemporaryPlacement)
                    Tiles[i].TileState = Tile.State.TestPlace;
                else if (player == Placement.Game.Player1Board.Player)
                    Tiles[i].TileState = Tile.State.Placed;
                else
                    Tiles[i].TileState = Tile.State.Occupied;

                // Set the tile's ship to this ship
                Tiles[i].Ship = this;

                // Determine the direction the ship is oriented to figure out which tile to access next
                if (direction == Direction.Left)
                    x--;
                else if (direction == Direction.Right)
                    x++;
                else if (direction == Direction.Up)
                    y--;
                else if (direction == Direction.Down)
                    y++;

            }

        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (Tile t in Tiles)
                        if (t != null)
                            t.TileState = Tile.State.Empty;

                    Tiles = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Size of the Ship
        /// </summary>
        public int Size { get; protected set; }
        /// <summary>
        /// If the ship has sunk or not
        /// </summary>
        public bool IsSunk { get; private set; } = false;
        /// <summary>
        /// Path to ship texture
        /// </summary>
        public string Texture { get; protected set; }

        /// <summary>
        /// Player the ship belongs to
        /// </summary>
        public Player Player { get; protected set; }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the number of tiles that are hit or sank
        /// </summary>
        /// <returns>An integer of the number of tiles that have been hit</returns>
        public int GetTilesHit()
        {
            return Tiles.Count(t => t.TileState == Tile.State.Hit || t.TileState == Tile.State.Sank);
        }

        /// <summary>
        /// Marks the target as "hit". Triggers sink if all tiles are hit
        /// </summary>
        /// <returns></returns>
        public bool Hit(int x, int y)
        {
            Tile tile = Tiles.FirstOrDefault(t => t.X == x && t.Y == y);
            if (tile == null) return false;

            tile.TileState = tile.IsOccupied() ? Tile.State.Hit : Tile.State.EmptyHit;

            if (GetTilesHit() >= Size && !IsSunk)
            {
                IsSunk = true;
                foreach (Tile t in Tiles) t.TileState = Tile.State.Sank;
                ShipSank?.Invoke(this, EventArgs.Empty);
            }

            return true;
        }

        public override string ToString()
        {
            string str = $"Ship coordinates: ({Tiles[0].X}, {Tiles[0].Y}) to ({Tiles[Tiles.Length - 1].X}, {Tiles[Tiles.Length - 1].Y});";
            if (IsSunk) str += " Ship has sunk;";

            return str;
        }

        #endregion

        #region Event Handlers

        public event EventHandler ShipSank;



        #endregion

    }
}
