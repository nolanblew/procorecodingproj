﻿using Procore_Coding_Project.Battleship.Pieces;

namespace Procore_Coding_Project.Battleship
{
    public class Game
    {

        #region Static Game Variables

        public readonly static int BoardSize = 10;
        public static bool isLogging = false;

#endregion

        #region Game variables

        public Board Player1Board;
        public Board Player2Board;
        public Board CurrentBoard => Turn%2 == 1 ? Player1Board : Player2Board;

        public int Turn = 1;
        public bool MyTurn => Turn%2 == 0;
        public bool IsActive = false;

        public bool IsPlacement => ShipSelect < 4;
        public int ShipSelect = -1;
        public ShipBase SelectedShipToPlace;

        #endregion


        #region Methods

        public void NewGame()
        {
            Player1Board = new Board();
            Player2Board = new Board();
            Turn = 1;
        }

        /// <summary>
        /// Iterates to the next placement of the ship, or starts the game otherwsie
        /// </summary>
        public void NextPlacement()
        {
            ShipSelect++;
        }

        public void NextTurn()
        {
            Turn++;
            if (MyTurn)
            IsActive = true;
        }

        public bool Fire(int x, int y)
        {
            IsActive = false;
            return CurrentBoard.Fire(x, y);
        }
        

        #endregion


    }
}
