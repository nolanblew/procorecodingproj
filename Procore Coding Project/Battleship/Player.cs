﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Procore_Coding_Project.Battleship.Pieces;

namespace Procore_Coding_Project.Battleship
{
    public class Player : INotifyPropertyChanged
    {
        
        #region Properties

        public string Name { get; set; }
        
        // Ships
        public Destroyer Destroyer { get; set; }
        public Submarine Sub { get; set; }
        public Pieces.Battleship Battleship { get; set; }

        /// <summary>
        /// Gets the total amount of ships that have been sunk
        /// </summary>
        public int ShipsSank
        {
            get
            {
                int sum = 0;
                if (Destroyer != null && Destroyer.IsSunk) sum++;
                if (Sub != null && Sub.IsSunk) sum++;
                if (Battleship != null && Battleship.IsSunk) sum++;

                return sum;
            }
        }

        private int _tokens = 0;
        public int Tokens
        {
            get { return _tokens; }
            set
            {
                if (_tokens != value)
                {
                    _tokens = value;
                    OnPropertyChanged("Tokens");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;



        #endregion

        #region Methods

        public bool HasWon()
        {
            OnPropertyChanged("ShipsSank");
            return Destroyer.IsSunk && Sub.IsSunk && Battleship.IsSunk;
        }



        void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        #endregion
    }
}
