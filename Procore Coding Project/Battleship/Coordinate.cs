﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procore_Coding_Project.Battleship
{
    public struct Coordinate
    {

        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }


        #region Overrides

        public override bool Equals(object obj)
        {
            if (!(obj is Coordinate)) return false;

            Coordinate c = (Coordinate)obj;
            return (X == c.X && Y == c.Y);
        }

        public override string ToString()
        {
            return $"({X},{Y})";
        }

        public static bool operator ==(Coordinate a, Coordinate b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Coordinate a, Coordinate b)
        {
            return !a.Equals(b);
        }

        public static Coordinate operator +(Coordinate a, Coordinate b)
        {
            return new Coordinate(a.X + b.X, a.Y + b.Y);
        }

        public static Coordinate operator -(Coordinate a, Coordinate b)
        {
            return new Coordinate(a.X - b.X, a.Y - b.Y);
        }

#endregion

    }
}
