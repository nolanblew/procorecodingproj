﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procore_Coding_Project.Battleship
{
    public static class Extentions
    {

        #region 2D Array Extentions

        public static T[,] To2DArray<T>(this List<List<T>> arrayList)
        {
            T[,] rtn = new T[arrayList.Count, arrayList[0].Count];

            try
            {
                for (int y = 0; y < arrayList[0].Count; y++)
                    for (int x = 0; x < arrayList.Count; x++)
                        rtn[x, y] = arrayList[x][y];

            }
            catch
            {
                throw;
            }

            return rtn;

        }

        public static T[][] ToMultidimentionalArray<T>(this List<List<T>> arrayList)
        {
            T[][] rtn = new T[arrayList.Count][];

            for (int x = 0; x < arrayList.Count; x++)
                rtn[x] = arrayList[x].ToArray();

            return rtn;
        }

        public static List<List<T>> ToDoubleList<T>(this T[,] arrayList)
        {
            List<List<T>> rtn = new List<List<T>>();

            for (int y = 0; y < arrayList.GetLength(0); y++)
            {
                List<T> lst = new List<T>();

                for (int x = 0; x < arrayList.GetLength(1); x++)
                    lst.Add(arrayList[x, y]);

                rtn.Add(lst);
            }

            return rtn;
        }

        public static List<List<T>> ToDoubleList<T>(this T[][] arrayList)
        {
            List<List<T>> rtn = new List<List<T>>();

            for (int y = 0; y < arrayList.Length; y++)
            {
                List<T> lst = new List<T>();

                for (int x = 0; x < arrayList[0].Length; x++)
                    lst.Add(arrayList[x][y]);

                rtn.Add(lst);
            }

            return rtn;
        }

        public static ObservableCollection<ObservableCollection<T>> ToDoubleObservableCollection<T>(this T[,] arrayList)
        {
            ObservableCollection<ObservableCollection<T>> rtn = new ObservableCollection<ObservableCollection<T>>();

            for (int y = 0; y < arrayList.GetLength(0); y++)
            {
                ObservableCollection<T> lst = new ObservableCollection<T>();

                for (int x = 0; x < arrayList.GetLength(1); x++)
                    lst.Add(arrayList[x, y]);

                rtn.Add(lst);
            }

            return rtn;
        }

        public static ObservableCollection<ObservableCollection<T>> ToDoubleObservableCollection<T>(this T[][] arrayList)
        {
            ObservableCollection<ObservableCollection<T>> rtn = new ObservableCollection<ObservableCollection<T>>();

            for (int y = 0; y < arrayList.Length; y++)
            {
                ObservableCollection<T> lst = new ObservableCollection<T>();

                for (int x = 0; x < arrayList[0].Length; x++)
                    lst.Add(arrayList[x][y]);

                rtn.Add(lst);
            }

            return rtn;
        }

        public static Coordinate ToCoordinate(this Direction direction)
        {
            if (direction == Direction.Left)
                return new Coordinate(-1,0);
            if (direction == Direction.Up)
                return new Coordinate(0,-1);
            if (direction == Direction.Right)
                return new Coordinate(1,0);
            if (direction == Direction.Down)
                return new Coordinate(0,1);

            return new Coordinate(0,0);
        }

        #endregion

    }
}
