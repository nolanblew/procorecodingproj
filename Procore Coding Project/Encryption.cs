﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Procore_Coding_Project
{
    public static class Encryption
    {

        private static int _privateKey;
        public static int PublicKey { get; private set; }

        static Random _rnd = new Random();

        public static async Task GeneratePublicPrivateKey()
        {
            var primes = await Eratosthenes.FindPrimes(Battleship.Board.MaxPrime);

            _privateKey = primes[_rnd.Next(0, primes.Count)];
            primes.Remove(_privateKey);

            PublicKey = primes[_rnd.Next(0, primes.Count)];
        }

        /// <summary>
        /// Encrypts a message and uses the previously-generated public and private key
        /// </summary>
        /// <param name="text">The text to encrypt</param>
        /// <returns>The encrypted string</returns>
        public static string Encrypt(string text)
        {
            return Encrypt(text, PublicKey);
        }

        /// <summary>
        /// Encrypts a message and uses the previously-generated private key against your public key
        /// </summary>
        /// <param name="text">The text to encrypt</param>
        /// <param name="publicKey">The public key provided</param>
        /// <returns>The encrypted string</returns>
        public static string Encrypt(string text, int publicKey)
        {
            return Encrypt(text, _privateKey, publicKey);
        }

        /// <summary>
        /// Encrypts a message based off a public key and a private key
        /// </summary>
        /// <param name="text">The text to encrypt</param>
        /// <param name="privateKey">The private key (as an int)</param>
        /// <param name="publicKey">The public key (as an int)</param>
        /// <returns>The encrypted string</returns>
        public static string Encrypt(string text, int privateKey, int publicKey)
        {
            int combined = privateKey * publicKey;

            // Encrypt the text
            // NOTE: For general demonstrational purposes, encryption is very simplistic.
            // This is simply to show how prime numbers are a part of encryption

            // First, add text to it
            text = $"battleship-{text}.encryption;";
            text = new string(text.Reverse().ToArray());
            text = $"bs{text}55";

            // Change each character
            char[] txt = new char[text.Length];
            for(int i = 0; i < text.Length; i++)
            {
                int chr = text[i];
                chr += combined;
                chr -= Math.Min(privateKey, publicKey);
                txt[i] = (char)chr;
            }

            return new string(txt);
        }


        /// <summary>
        /// Will decrpyt the string based on the provided public key, and stored private key
        /// </summary>
        /// <param name="text">The text to decrypt</param>
        /// <param name="publicKey">The public key given to you</param>
        /// <returns>Decrypted string</returns>
        public static string Decrypt(string text, int publicKey)
        {
            return Decrypt(text, _privateKey, publicKey);
        }

        /// <summary>
        /// Will decrypt the string based on the provided public and private key
        /// </summary>
        /// <param name="text">The text to decrypt</param>
        /// <param name="privateKey">The private key</param>
        /// <param name="publicKey">The public key given to you</param>
        /// <returns>Decrypted string</returns>
        public static string Decrypt(string text, int privateKey, int publicKey)
        {
            int combined = privateKey * publicKey;

            // Frist, Attempt to decrypt the text
            char[] txt = new char[text.Length];
            for (int i = 0; i < text.Length; i++)
            {
                int chr = text[i];
                chr += Math.Min(privateKey, publicKey);
                chr -= combined;
                txt[i] = (char)chr;
            }

            string rtn = new string(txt);

            // Remove edges
            rtn = rtn.Substring(2);
            rtn = rtn.Substring(0, rtn.Length - 2);

            rtn = new string(rtn.Reverse().ToArray());
            rtn = rtn.Substring(11);
            rtn = rtn.Substring(0, rtn.Length - 12);

            return rtn;
        }

        /// <summary>
        /// Will determine if given input is the private key. NOT FOR USER USE
        /// </summary>
        /// <param name="privateKey">The key to test if it is the Private Key</param>
        /// <returns>True if the given key is the private key, false if not</returns>
        public static bool IsPrivateKey(int privateKey)
        {
            return privateKey == _privateKey;
        }

    }
}
