# README #

Hello. Thank you for taking the time to look at my coding project. This readme covers a couple of notes as well as the instructions on how to use my program). Please, feel free to take a look at the finished product as well as the code.

## A few things to note ##
There are a few notes for this project. First off, due to strenuous time constraints (and an unfortunate instance over the last few weeks that occupied my time), the UI on this is not polished. In my work on Windows 10 applications, I follow Microsoft's UX design guidelines as much as possible. Take a look at the [Windows 10 UX Design Guidelines](https://msdn.microsoft.com/library/windows/apps/hh465424.aspx).

This program was developed as a WPF application using C# and .NET 4.5.1. I used WPF as it works on all Windows machines (and they don't have to have a dev license or I don't have to actually submit it to the store), while keeping the general XAML layout templates in tact.

## What did I create? ##

On top of a very simplistic prime-number generator, I decided to apply the world's best use of prime numbers (cryptography) in a fun way: Battleship. While the graphics are very basic and minimal, the classic game of Battleship is still easily recognizable. Just with a twist:

You have found out that the enemy is sending encrypted messages to each other. You can intercept their messages, but you will need a key to decrypt the messages. You have easily come across their public key (shown to you on screen), but finding their private key is a challenge.

Using Eratosthenes' Sieve algorithm, the system is able to generate a list of prime numbers (for purposes of this game, with an upper limit of 120) and then select 2 of them - one as the private and one as the public key. This way, it is impossible to decode the message without both keys (even if you could see the algorithm to encode/decode the message). Follow the instructions under "Battleship" section of this page to find out how to play the game.

# Using my Program #

Upon entering the app, you will have two options - generate prime numbers or play Battleship. Selecting the option to generate prime numbers will bring you to a window in which you can enter the upper limit (the program limits upwards to 10,000,000, in order to not tax system resources too much or have to wait too long). The program will then use the sieve to generate all prime numbers up to that limit.

The other button, is to play Battleship!

## Battleship ##

### Screen Layout ###

This is your screen layout while playing the game. Note the red numbers.

![procore_main_sample.png](https://bitbucket.org/repo/R4rayK/images/180266798-procore_main_sample.png)

### Starting ###

To begin, read the instructions (#6) and click "Next"

After, you will be presented with the option to place your first ship: your Destroyer. Place the ship anywhere in a valid area on the screen (it will highlight in yellow). You can rotate by pressing the < or > keys (no SHIFT, so in reality , and .)  Click to place, and repeat for all 3 ships.

### Playing ###

You are first. Go ahead and click on any tile to fire (#3). If you hit, the tile will turn red. If you miss, it will turn light blue. And if you sink a ship, the ship will turn dark red.

You can keep track of the numbers of ships sunk at the top (#1). Your tokens (#2) are for decrypting their messages (#5). More on that later.

After you click, it will take 2 seconds before the computer will fire on your board. There is nothing you can do but watch and hope that your ships aren't hit.

Tile colors:
* Yellow - Placing/Selection (waiting for an action)
* White - Empty -- nothing there
* Light Blue - Fired and missed
* Solid Red - Fired and hit
* Dark Red - Entire ship is sunk

### Decrypting Messages ###

Decrypting messages is simple. You will need tokens to decrypt. You get 1 token for each time you fire, and 1 extra token for every hit. Each decryption attempt costs 1 token, even if you've already decrypted a previous message before.

Clicking on "Prime Generator" will open a new window. In this window, you have the ability to get a list of all the prime numbers up to a given limit (the game goes up to 120). It costs tokens to generate prime numbers, particularly higher ones so use it wisely.

The real beauty is the Auto Find. It costs twice the price, but after you generate up to a given limit, auto-find will attempt to see if it can decrypt the message using any of the generated prime numbers. If it can, the current message will be decrypted. However, it will NOT reveal what the correct prime number was for decryption.

**NOTE:** The tiles are 1-based tiles from the top left corner. So 2,7 would be the 2nd column, 7th row down.

Prices:
* Simple decryption (main window): 1 token
* Listing prime numbers: 2 tokens + 1 token for every 40. i.e. 35 is 3 tokens (2 + 1 (35 is less than 40)); 92 is 5 tokens (2 + 3 (92 --> above 80, below 120))
* Auto-Find: Listing price times 2. i.e. 35 is 6 tokens (listing 35 is 3 tokens), and 92 is 10 tokens

# Game Technicalities and Code #

## Primes and Cryptography ##

All prime numbers are generated in Eratosthenes.cs. It is a static class so do not attempt to declare an instance of it. The one public function is FindPrimes(int) that returns  list of integers -- generated primes. This method is an asynchronous Task.

Encrypting and Decrypting the text is found in Encryption.cs. Very simple and basic encryption that I created, multiplying the private and public key, then messing a bit with the character value (no low-level bit shifting, etc, for example purposes). 

## Game ##

### Artificial Intelligence ###

The AI in this game is very simplistic. It randomly chooses a tile and direction to place its ships, and randomly chooses a square to attack (not based on logic). If it hits a square, it will follow that lead by looking around to find other squares to hit, like humans. It will continue to do so until it either sinks the ship, or becomes confused, in which both cases it will go back to randomly firing.

### Controls / UI ###

This game uses basic XAML elements. Each tile is from a custom user control named "Tile.xaml" that gets spread out in an array to form the board. The color of the tile is a binding to the Tile.cs class' "TileState". Therefore, no programmatic code is needed anywhere except to change the state of the title you are dealing with. Many other elements have a UI Binding attached to them as well


# Questions #

If you have any questions, or run into any problems, please do not hesitate to contact me. This program requires no packages or extra prerequisites, and only requires a minimum of .NET 4.5.1 in order to run.

** ENJOY! **